// Type Annotations
let apples = 5

let myName = 'Dom'
console.log(typeof myName)
let hasName = false
console.log(hasName)
let nullObject = undefined
console.log(nullObject)

// built in objects
let currentDate: Date = new Date()
console.log(currentDate)

// Array
let colors: string[] = ['Red', 'Green', 'Purple']
console.log(colors)

let truths = [true, true, true]
console.log(truths)

// Classes
class Car {}

let car: Car = new Car()
console.log(car)

// Object literal
let point: { x: number; y: number } = {
  x: 10,
  y: 20
}

const logNumber: (i: number) => void = (i: number) => {
  return i
}

const result: any = logNumber(33)
console.log(result)

const json = '{"x": 10, "y": 20}'
const coordinates: { x: number; y: number } = JSON.parse(json)
console.log(coordinates)

let words = ['red', 'green', 'dom']
let foundWord: boolean

words.find((word) => {
  word === 'dom' ? (foundWord = true) : 'No good...'
})

// Variable can not be inferred
const countAboveZero = () => {
  let numbers = [-10, -1, 12]
  let numberAboveZero: boolean | number = false
  for (let number in numbers) {
    numberAboveZero = numbers[number]
    console.log(numberAboveZero)
  }
  return numberAboveZero
}

const countResult = countAboveZero()
console.log(countResult)
