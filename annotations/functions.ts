const add = (a: number, b: number): number => {
  return a + b
}

const addResult = add(2, 3)
console.log(addResult)

const subtract = (a: number, b: number): number => a - b
const subtractResult = subtract(10, 3)

const multiply = (a: number, b: number): number => a * b
const multiplyResult = multiply(20, 20)

const divide = (a: number, b: number): number => a / b
const divideResult = divide(20, 20)

export default add

const logger = (message: string): void => console.log(message)
const messageLogger = logger('MESSAGE')

const throwError = (message: string): void => {
  if (!message) {
    throw new Error(message)
  }
}

const todaysWeather = {
  date: new Date(),
  weather: 'sunny'
}

const logWeather = ({ date, weather }: { date: Date; weather: string }): void => {
  console.log(date)
  console.log(weather)
}

logWeather(todaysWeather)
