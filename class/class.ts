// classes are meant to be a blueprint
class Vehicle {
  constructor(public color: string) {}
  public drive(): void {
    console.log(`go go`)
  }
  protected honk(): void {
    console.log(`beep beep`)
  }
}

const vehicle = new Vehicle('Gray')
console.log(vehicle.color)

class Car extends Vehicle {
  constructor(public wheels: number, color: string) {
    super(color)
  }

  drive(): void {
    console.log(`vroom`)
  }
  startDrivingProcess(): void {
    this.drive()
    this.honk()
  }
}

const car = new Car(4, 'Green')
console.log(car.startDrivingProcess())
