import axios from 'axios'

interface Todo {
  id: number
  title: string
  completed: boolean
}

const URL = 'https://jsonplaceholder.typicode.com/todos/1'
axios.get(URL).then((response) => {
  const todo = response.data as Todo
  const id = todo.id
  const title = todo.title
  const completed = todo.completed

  logTodo(id, title, completed)
})

const logTodo = (id: number, title: string, completed: boolean) => {
  console.log(`
    The todo with id: ${id}
    Has a title: ${title}
    Is it completed: ${completed}
  `)
}
