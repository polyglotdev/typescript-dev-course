const today = new Date()
today.getMonth()

interface Person {
  name: string
  age: number
  location: string
  occupation: string
}

const personGrabber = (name: string, age: number, location: string, occupation: string) => {
  const response = {
    data: {
      name: 'Dom',
      age: 36,
      location: 'St. Louis',
      occupation: 'Software Engineer'
    }
  }
  console.log(response.data)
  const newThanks = response.data as Person
  console.log(newThanks)
  return name
}

const personResult = personGrabber('Dominique', 37, 'Denver, Colorado', 'Software Engineer')
console.log(personResult)

class Color {
  flavor: 'deep'
}

const red = new Color()
console.log(red.flavor)
