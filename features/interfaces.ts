interface Reportable {
  summary(): string
}

const fountainDrink = {
  color: 'brown',
  carbonated: true,
  gramsOfSugar: 40,
  summary(): string {
    return `My drink has ${this.gramsOfSugar} sugar in it.`
  }
}
const mazdaCX5 = {
  manufacturer: 'Mazda',
  modelName: 'CX-5',
  year: new Date(),
  color: 'White',
  broken: false,
  summary() {
    return `Name: ${this.manufacturer} ${this.modelName}`
  }
}
const printVehicle = (item: Reportable): string => {
  return item.summary()
}

printVehicle(mazdaCX5) /* ? */
printVehicle(fountainDrink) /* ? */
